# Description
Blog is a very simple and clean-design blog system implemented with Spring Boot. It's one of my learning projects to explore awesome features in Spring Boot web programming.

## SpringBlog is powered by many powerful frameworks and third-party projects:
- Spring Boot and many of Spring familiy.
- Hibernate(JPA 2) and MySql.
- HikariCP - A solid high-performance JDBC connection pool.
- H2 - Available in embedded and server modes; in-memory databases
- Bootstrap - A very popular and responsive front-end framework
- Txtmark - Another markdown processor for the JVM.
- Websocket - STOMP - Sockjs
- RabbitMQ - Message Queue.
- Thymeleaf - A modern server-side Java template engine for both web and standalone environments.

## Features include (all are processed in real time)
- Login / Logout (with social).
- Create Post then push notify for all user online in website.
- Post a comment:
    + Notify the poster.
    + Notify all users commented.
    + Reply comment.
- Find post, top post, new post.
- Edit Profile.
- Managerment user for admin.

## Next feature.
- Show user off/online.
- Create room and chat room.
- Flow user.
- Chat one to one.
- Convert mysql to mongodb.
- The above features have been built on NodeJS.([Chatbox](https://gitlab.com/DbCooper/Chatbox))
  

