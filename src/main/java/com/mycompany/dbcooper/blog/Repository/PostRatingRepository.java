package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.PostRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRatingRepository extends JpaRepository<PostRating,Long>{
    @Query("SELECT pr FROM PostRating pr WHERE pr.user.Id= :userId AND pr.post.Id= :postId")
    PostRating findByUserRating(@Param("postId") Long postId, @Param("userId") Long userId);
}
