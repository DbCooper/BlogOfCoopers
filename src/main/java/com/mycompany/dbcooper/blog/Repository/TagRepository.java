package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag,Long>{
    Tag findByName(String name);
}
