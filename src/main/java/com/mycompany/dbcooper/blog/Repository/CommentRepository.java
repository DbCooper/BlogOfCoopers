package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long>{
}
