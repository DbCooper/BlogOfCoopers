package com.mycompany.dbcooper.blog.Repository;
import com.mycompany.dbcooper.blog.Model.PersistentLogin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository("tokenRepository")
public class HibernateTokenRepositoryImp implements PersistentTokenRepository{
    @Autowired
    private final PersistentLoginRepository persistentLoginRepository;
    public HibernateTokenRepositoryImp(PersistentLoginRepository persistentLoginRepository){
        this.persistentLoginRepository=persistentLoginRepository;
    }
    static final Logger logger = LoggerFactory.getLogger(HibernateTokenRepositoryImp.class);
    @Override
    public void createNewToken(PersistentRememberMeToken persistentRememberMeToken) {
        logger.info("Create Token for username: {}",persistentRememberMeToken.getUsername());
        PersistentLogin persistent=new PersistentLogin();
            persistent.setSeries(persistentRememberMeToken.getSeries());
            persistent.setToken(persistentRememberMeToken.getTokenValue());
            persistent.setUsername(persistentRememberMeToken.getUsername());
            persistent.setLast_used(persistentRememberMeToken.getDate());
            persistentLoginRepository.saveAndFlush(persistent);
    }

    @Override
    public void updateToken(String seriesId, String token, Date date) {
        logger.info("Updating Token for seriesId : {}", seriesId);
        PersistentLogin persistent=persistentLoginRepository.findBySeries(seriesId);
        persistent.setToken(token);
        persistent.setLast_used(date);
        persistentLoginRepository.saveAndFlush(persistent);
        logger.info("Updated token for: {}",persistent.toString());

    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        logger.info("Fetch Token if any for seriesId : {}", seriesId);
        PersistentLogin persistent=persistentLoginRepository.findBySeries(seriesId);
        if (persistent!=null){
            logger.info("Token is: {}",persistent.getToken());
            return new PersistentRememberMeToken(persistent.getUsername(),persistent.getSeries(),persistent.getToken(),persistent.getLast_used());
        }else{
            logger.info("Token not found.");
            return null;
        }
    }

    @Override
    public void removeUserTokens(String username) {
        logger.info("Removing Token if any for user: {}",username);
        List<PersistentLogin> persistent=persistentLoginRepository.findByUsername(username);
        if (persistent!=null){
            persistentLoginRepository.delete(persistent);
             persistentLoginRepository.flush();
             logger.info("rememberMe was selected: {}",username);
        }

    }
}
