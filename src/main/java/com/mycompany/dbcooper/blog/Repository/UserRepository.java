package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{
    User findUserByUsername(String username);
    User findUserByEmail(String email);

    @Query("SELECT u FROM User u WHERE u.username LIKE :username OR :username IS NULL OR :username = '' ORDER BY u.registrationDate DESC ")
    Page<User> findAllByUsername(@Param("username") String username, Pageable pageable);
}
