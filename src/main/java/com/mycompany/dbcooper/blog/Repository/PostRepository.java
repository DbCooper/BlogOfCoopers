package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Long>{
    Page<Post> findByHiddenIsFalse(Pageable pageable);

    List<Post> findByHiddenIs(boolean hidden,Pageable pageable);

    @Query("SELECT p FROM Post p WHERE :totalTags=(SELECT COUNT(DISTINCT t.id) FROM Post p2 JOIN p2.tags t WHERE LOWER(t.name) in :tags AND p2 = p)")
    Page<Post> findByTags(@Param("tags") List<String> tags,@Param("totalTags")Long totalTags,Pageable pageable);

    @Query("SELECT p FROM Post  p WHERE :totalTags=(SELECT COUNT( DISTINCT t.id) FROM Post p2 JOIN p2.tags t WHERE p2.hidden=false AND LOWER(t.name) in :tags AND p2=p)")
    Page<Post> findByTagsAndNotHidden(@Param("tags") List<String> tags,@Param("totalTags") Long totalTags,Pageable pageable);

    @Query("SELECT p FROM Post p JOIN p.postRatings r WHERE p.hidden=false GROUP BY p ORDER BY SUM(r.value) DESC ")
    List<Post> topPost(Pageable pageable);

 
 }
