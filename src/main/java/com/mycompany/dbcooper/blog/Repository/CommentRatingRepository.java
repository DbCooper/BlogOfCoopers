package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.CommentRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRatingRepository extends JpaRepository<CommentRating,Long>{
    @Query("SELECT cr FROM CommentRating cr WHERE cr.user.Id= :userId AND cr.comment.Id= :commentId")
    CommentRating findByUserRating(@Param("userId") Long userId, @Param("commentId") Long commentId);
}
