package com.mycompany.dbcooper.blog.Repository;

import com.mycompany.dbcooper.blog.Model.PersistentLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersistentLoginRepository extends JpaRepository<PersistentLogin,String>{
    @Query("SELECT p FROM PersistentLogin p WHERE p.username =:username")
    List<PersistentLogin> findByUsername(@Param("username") String username);
    PersistentLogin findBySeries(String seriesId);
}
