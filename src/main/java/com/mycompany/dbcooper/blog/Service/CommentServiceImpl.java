package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.ActionExpiredException;
import com.mycompany.dbcooper.blog.Exception.AlreadyVotedException;
import com.mycompany.dbcooper.blog.Exception.AuthException;
import com.mycompany.dbcooper.blog.Exception.ForbiddenException;
import com.mycompany.dbcooper.blog.Model.Comment;
import com.mycompany.dbcooper.blog.Model.CommentRating;
import com.mycompany.dbcooper.blog.Model.Rating;
import com.mycompany.dbcooper.blog.Model.User;
import com.mycompany.dbcooper.blog.Repository.CommentRatingRepository;
import com.mycompany.dbcooper.blog.Repository.CommentRepository;
import com.mycompany.dbcooper.blog.Repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service("CommentService")
@Transactional
public class CommentServiceImpl implements CommentService{
    private final Logger LOGGER= LoggerFactory.getLogger(CommentServiceImpl.class);
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private CommentRatingRepository commentRatingRepository;

    @Autowired
    private PostRepository postRepository;

    @Override
    public Comment getComment(Long id) {
        
        return commentRepository.findOne(id);
    }

    @Override
    public Comment saveNewComment(Comment comment, Long postId, Long parentId) {
        Long commentId=null;
        try{
            if (parentId!=null){
               Comment parentComment=commentRepository.findOne(parentId);
               int level=parentComment.commentLevel();
               comment.setParentComment(level>3?parentComment.getParentComment():parentComment);
            }
            comment.setDateTime(LocalDateTime.now());
            comment.setPost(postRepository.findOne(postId));
            comment.setUser(userService.currentUser());
            Comment commentNew= commentRepository.saveAndFlush(comment);
            LOGGER.info("Success! Has created a new comment with CommentId: {}"+commentNew.getId());
            return commentNew;
        }catch (Exception ex){
            LOGGER.info("Error! "+ex.getMessage());
            return null;
        }
    }

    @Override
    public void updateComment(Comment comment) throws ActionExpiredException, AuthException {
        Comment commentStore=commentRepository.findOne(comment.getId());
        User user=userService.currentUser();
        if (!commentStore.getUser().getId().equals(user.getId())){
            throw new AuthException("Error! You aren't permission.");
        }
        if (!commentStore.userCanEdit()){
            throw new ActionExpiredException("Error! Update time exceeded");
        }
        commentStore.setCommentText(comment.getCommentText());
        commentStore.setModifiedDateTime(LocalDateTime.now());
        commentRepository.saveAndFlush(comment);
    }

    @Override
    public void deleteComment(Long commmentId) throws AuthException, ActionExpiredException {
        Comment comment=commentRepository.getOne(commmentId);
        User user=userService.currentUser();

        if (!userService.isAdmin() && !comment.getUser().getId().equals(user.getId())){
            throw new AuthException("Error! You aren't permission.");
        }

        if (!userService.isAdmin() && !comment.userCanDelete()){
            throw new ActionExpiredException("Error! Delete time exceeded");
        }
        commentRepository.findOne(commmentId);
        commentRepository.flush();
    }

    @Override
    public void vote(Long commentId, boolean like) throws ForbiddenException, AlreadyVotedException {
        User currentUser=userService.currentUser();
        Comment comment=commentRepository.getOne(commentId);
        if (currentUser.getId().longValue() == comment.getUser().getId().longValue()) {
            throw new ForbiddenException("Cannot vote for own comments");
        }

        CommentRating rating = commentRatingRepository.findByUserRating(currentUser.getId(),commentId);

        if (rating != null) {
            throw new AlreadyVotedException("Cannot vote more than once");
        }
        rating=new CommentRating();
        rating.setComment(comment);
        rating.setUser(currentUser);
        rating.setValue(like? Rating.LIKE_VALUE:Rating.DISLIKE_VALUE);
        commentRatingRepository.saveAndFlush(rating);
    }
}
