package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.ActionExpiredException;
import com.mycompany.dbcooper.blog.Exception.AlreadyVotedException;
import com.mycompany.dbcooper.blog.Exception.AuthException;
import com.mycompany.dbcooper.blog.Exception.ForbiddenException;
import com.mycompany.dbcooper.blog.Model.Comment;

public interface CommentService {
    Comment getComment(Long commentId);

    Comment saveNewComment(Comment comment,Long postId, Long parentId);

    void updateComment(Comment comment) throws ActionExpiredException, AuthException;

    void deleteComment(Long commentId) throws AuthException,ActionExpiredException;

    void vote(Long commentId, boolean like) throws ForbiddenException,AlreadyVotedException;

}
