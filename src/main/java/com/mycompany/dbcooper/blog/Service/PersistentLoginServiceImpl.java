package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Model.PersistentLogin;
import com.mycompany.dbcooper.blog.Repository.PersistentLoginRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("persistentLoginService")
@Transactional
public class PersistentLoginServiceImpl implements PersistentLoginService{
    @Autowired
    private PersistentLoginRepository persistentLoginRepository;
    @Override
    public void savePersistentLogin(PersistentLogin persistentLogin) {
        persistentLoginRepository.saveAndFlush(persistentLogin);
    }

    @Override
    public List<PersistentLogin> findByUsernamge(String username) {
        return persistentLoginRepository.findByUsername(username);
    }

    @Override
    public PersistentLogin findBySeriesId(String seriesId) {
        return persistentLoginRepository.findBySeries(seriesId);
    }

    @Override
    public void deletePersistentLogin(PersistentLogin persistentLogin) {
        persistentLoginRepository.delete(persistentLogin);
        persistentLoginRepository.flush();
    }
}
