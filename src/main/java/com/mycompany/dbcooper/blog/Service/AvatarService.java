package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.UnsupportedFormatException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface AvatarService {
    UploadAvatarInfo uploadAvavatar(MultipartFile file) throws IOException,UnsupportedFormatException;
}
