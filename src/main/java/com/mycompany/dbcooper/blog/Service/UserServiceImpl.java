package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.AuthException;
import com.mycompany.dbcooper.blog.Exception.ResourceNotFoundException;
import com.mycompany.dbcooper.blog.Model.Role;
import com.mycompany.dbcooper.blog.Model.User;
import com.mycompany.dbcooper.blog.Repository.RoleRepository;
import com.mycompany.dbcooper.blog.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    private AuthenticationTrustResolver authenticationTrustResolver;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public void register(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        user.getRoles().add(roleRepository.findByName("ROLE_USER"));

        user.setEnabled(true);

        user.setRegistrationDate(LocalDateTime.now());

        userRepository.saveAndFlush(user);
    }

    @Override
    public boolean usernameExist(String username) {
        return findByUsername(username)!=null;
    }

    @Override
    public boolean emailExist(String email) {
        return findByEmail(email)!=null;
    }

    @Override
    public void changeEmail(String newEmail, String oldPassword) throws AuthException {
        User user = currentUser();
        if (!passwordEncoder.matches(oldPassword,user.getPassword())){
            throw new AuthException("Password not match!");
        }
        user.setEmail(newEmail);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) throws AuthException {
        User user = currentUser();
        if (!passwordEncoder.matches(oldPassword,user.getPassword())){
            throw new AuthException("Password not match!");
        }
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.saveAndFlush(user);
    }

    @Override
    public void changeProfileInfo(User newProfileInfo) {
        User user = currentUser();

        user.setAboutText(newProfileInfo.getAboutText());

        user.setWebsiteLink(newProfileInfo.getWebsiteLink());

        userRepository.saveAndFlush(user);
    }

    @Override
    public void changeAvatar(UploadAvatarInfo uploadedAvatarInfo) {
        User user = currentUser();

        user.setBigAvatarLink(uploadedAvatarInfo.bigImageLink);

        user.setSmallAvatarLink(uploadedAvatarInfo.smallImageLink);

        userRepository.saveAndFlush(user);
    }

    @Override
    public void removeAvatar() {
        User user= currentUser();
        user.setBigAvatarLink(null);
        user.setSmallAvatarLink(null);
    }

    @Override
    public void authenticate(User user) {
        UserDetails userDetails=customUserDetailsService.loadUserByUsername(user.getUsername());
        Authentication authentication=new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    public boolean isAuthenticated() {
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
        return authenticationTrustResolver.isAnonymous(authentication);
    }

    @Override
    public boolean isAdmin() {

        User user = currentUser();

        return user!=null && user.hasRole("ROLE_ADMIN");
    }

    @Override
    public User currentUser() {
        if (isAuthenticated()){
            return null;
        }
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return findByUsername(username);
    }

    @Override
    public void addRole(String roleName, Long userId) throws AuthException {
        if(!isAdmin()){
           throw new AuthException("Not permissin!!!");
        }
        Role role=roleRepository.findByName(roleName);
        User user = userRepository.findOne(userId);
        if (role==null || user==null) {
            throw new ResourceNotFoundException();
        }
        user.getRoles().add(role);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void removeRole(String roleName, Long userId) throws AuthException {
        if(!isAdmin()){
            throw new AuthException("Not permissin!!!");
        }
        Role role=roleRepository.findByName(roleName);
        User user = userRepository.findOne(userId);
        if (role==null || user==null) {
            throw new ResourceNotFoundException();
        }
        user.getRoles().remove(role);
        userRepository.saveAndFlush(user);
    }

    @Override
    public Page<User> findAllUserByUsername(String username, int pageNumber, int pageIndex) {
        Pageable pageable=new PageRequest(pageIndex, pageNumber, Sort.Direction.DESC, "registrationDate");
        return userRepository.findAllByUsername(username, pageable);
    }
}
