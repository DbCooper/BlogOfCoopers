package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.UnsupportedFormatException;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service("avatarService")
public class AvatarServiceImpl implements AvatarService{
    private final Logger logger= LoggerFactory.getLogger(AvatarServiceImpl.class);
    @Value("${uploading.dirpath}")
    private String uploadingDirPath;

    @Autowired
    private FileNameGenerator fileNameGenerator;


    public final List<String> SUPPORTED_EXTENSIONS = Arrays.asList("jpg", "jpeg", "png");

    @Override
    public UploadAvatarInfo uploadAvavatar(MultipartFile file) throws IOException, UnsupportedFormatException {
        String originalPath = file.getOriginalFilename();
        int dot = originalPath.lastIndexOf('.');
        String getExtension = (dot == -1) ? "" : originalPath.substring(dot + 1);
        if (!SUPPORTED_EXTENSIONS.contains(getExtension)){
            throw new UnsupportedFormatException("File not format: "+file);
        }
        String nameFile=fileNameGenerator.getFileName(originalPath,"avatar");

        BufferedImage tempImage=ImageIO.read(file.getInputStream());
        BufferedImage bigImage= scalrAvatar(tempImage,160,160);
        BufferedImage smallImage=scalrAvatar(tempImage,28,28);

        String bigImageName = nameFile + "_big" + "." + getExtension;
        String smallImageName = nameFile + "_small" + "." + getExtension;


        File dirFile=new File("src"+File.separator+"main"+File.separator+"resources"+File.separator+"static"+File.separator+"uploads"+File.separator+"/avatars");
        if (!dirFile.exists()) {
           dirFile.mkdirs();
        }
        logger.info("Path save file: "+dirFile.getPath());
        ImageIO.write(bigImage,getExtension,new File(dirFile.getPath()+File.separator+bigImageName));
        ImageIO.write(smallImage,getExtension,new File(dirFile.getPath()+File.separator+smallImageName));
        return new UploadAvatarInfo(bigImageName,smallImageName);
    }
    private BufferedImage scalrAvatar(BufferedImage image, int width, int height){
        return Scalr.resize(image, Scalr.Mode.AUTOMATIC,width,height);
    }
}
