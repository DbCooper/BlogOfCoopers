package com.mycompany.dbcooper.blog.Service;

import org.springframework.stereotype.Service;


public interface FileNameGenerator {
    String getFileName(String filename, String prefix);
}
