package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Model.Role;
import com.mycompany.dbcooper.blog.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("roleService")
public class RoleServiceImpl implements RoleService{
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Role> GetAllRole() {
        return roleRepository.findAll();
    }

    @Override
    public Role findByName(String roleName) {
        return findByName(roleName);
    }
}
