package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Model.User;
import com.mycompany.dbcooper.blog.RabbitMQ.UserQueueRabbit;
import com.mycompany.dbcooper.blog.Repository.UserRepository;
import java.io.IOException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{
    static final org.slf4j.Logger logger= LoggerFactory.getLogger(CustomUserDetailsService.class);
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private HttpSession httpSession;
    
    @Autowired
    private UserQueueRabbit queueRabbit;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user =userRepository.findUserByUsername(username);
        logger.info("User: {}",user);
        if (user==null){
            logger.info("user not found");
            throw new UsernameNotFoundException("Username not found");
        }
        httpSession.setAttribute("userCurrent",user);
        try {
            queueRabbit.createNotifycationQueueUser(user.getId());
        } catch (IOException ex) {
            Logger.getLogger(CustomUserDetailsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),true,true,true,true,getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user){
        List<GrantedAuthority> authorities=new ArrayList<>();
        logger.info("Start add Role",user.getRoles());
        user.getRoles().stream().forEach(r->authorities.add(new SimpleGrantedAuthority(r.getName())));
        logger.info("List Role: {}",authorities);
        return authorities;
    }
}
