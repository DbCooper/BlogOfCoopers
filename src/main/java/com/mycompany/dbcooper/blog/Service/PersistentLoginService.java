package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Model.PersistentLogin;
import java.util.List;

public interface PersistentLoginService {
    void savePersistentLogin(PersistentLogin persistentLogin);
    List<PersistentLogin> findByUsernamge(String username);
    PersistentLogin findBySeriesId(String seriesId);
    void deletePersistentLogin(PersistentLogin persistentLogin);
}
