package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.AuthException;
import com.mycompany.dbcooper.blog.Model.User;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;

public interface UserService {
    User findByUsername(String username);

    User findByEmail(String email);

    void register(User user);

    boolean usernameExist(String username);

    boolean emailExist(String email);

    void changeEmail(String newEmail, String password)  throws AuthException;

    void changePassword(String oldPassword, String newPassword) throws AuthException;

    void changeProfileInfo(User newProfileInfo);

    void changeAvatar(UploadAvatarInfo uploadedAvatarInfo);

    void removeAvatar();

    void authenticate(User user);

    boolean isAuthenticated();

    boolean isAdmin();

    User currentUser();

    void addRole(String role,Long userId) throws AuthException;

    void removeRole(String role,Long userId) throws AuthException;
    @PreAuthorize("hasRole('ADMIN')")
    Page<User> findAllUserByUsername(String username, int pageNumber, int pageIndex);
}
