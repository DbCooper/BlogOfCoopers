package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.AlreadyVotedException;
import com.mycompany.dbcooper.blog.Model.Post;
import com.mycompany.dbcooper.blog.Model.PostEditDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface PostService {
    Post createPost(PostEditDto postEditDto);

    Post updatePost(PostEditDto postEditDto);

    PostEditDto editPost(Long postId);

    void deletePost(Long postId);

    Page<Post> getPostPage(int pageSize, int pageIndex);

    List<Post> getPostList(int pageSize, int pageIndex);

    List<Post> getTopPost();

    Page<Post> findByTags(String tag, int pageSize, int pageIndex);

    Post getPost(Long postId);

    void setPostVisibility(Long postId, boolean hide);

    void vote(Long postId, boolean like) throws AlreadyVotedException;
}
