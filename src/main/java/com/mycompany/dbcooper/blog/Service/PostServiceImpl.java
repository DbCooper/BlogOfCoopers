package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Exception.AlreadyVotedException;
import com.mycompany.dbcooper.blog.Model.*;
import com.mycompany.dbcooper.blog.Repository.PostRatingRepository;
import com.mycompany.dbcooper.blog.Repository.PostRepository;
import com.mycompany.dbcooper.blog.Repository.TagRepository;
import com.mycompany.dbcooper.blog.Util.MarkdownConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service("postService")
@Transactional
public class PostServiceImpl implements PostService{
    private static final Logger LOGGER= LoggerFactory.getLogger(PostServiceImpl.class);

    @Autowired
    private PostRatingRepository postRatingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private TagRepository tagRepository;
    @Override
    public Post createPost(PostEditDto postEditDto) {
        Post post=new Post();
        post=fillPost(post,postEditDto);
        Post postCreate=postRepository.saveAndFlush(post);
        LOGGER.info("Success! Post haven been posted with Id:  {}",postCreate.getId());
        return postCreate;
    }

    @Override
    public Post updatePost(PostEditDto postEditDto) {
        Post post = postRepository.findOne(postEditDto.getId());
        if (post==null){
            LOGGER.info("Error! Post not found with id: {}"+postEditDto.getId());
            return null;
        }
        post=fillPost(post,postEditDto);
        postRepository.saveAndFlush(post);
        return post;
    }

    @Override
    public PostEditDto editPost(Long postId) {
        Post post=postRepository.findOne(postId);
        if (post==null){
            LOGGER.info("Error! PostId not found:{}"+postId);
            return null;
        }
        PostEditDto postEditDto=new PostEditDto();
        postEditDto=convertToPostEditDto(post,postEditDto);
        return postEditDto;
    }

    @Override
    public void deletePost(Long postId) {
        LOGGER.info("Delete postId:{}"+postId);
        try{
            postRepository.delete(postId);
            LOGGER.info("Success! PostId {} haven been deleted."+postId);
        }catch (Exception ex){
            LOGGER.info("Error! "+ex.getMessage());
        }
    }

    @Override
    public Page<Post> getPostPage(int pageSize, int pageIndex) {
        Pageable pageable=new PageRequest(pageIndex,pageSize, Sort.Direction.DESC,"dateTime");
        if (userService.isAdmin()){
            return postRepository.findAll(pageable);
        }
        return postRepository.findByHiddenIsFalse(pageable);
    }

    @Override
    public List<Post> getPostList(int pageSize, int pageIndex) {
        Pageable pageable = new PageRequest(pageIndex,pageSize, Sort.Direction.DESC,"dateTime");
        
        return postRepository.findByHiddenIs(false,pageable);
    }

    @Override
    public List<Post> getTopPost() {
        Pageable pageable=new PageRequest(0,10);
        return postRepository.topPost(pageable);
    }

    @Override
    public Page<Post> findByTags(String tag, int pageSize, int pageIndex) {
        Pageable pageable=new PageRequest(pageIndex,pageSize, Sort.Direction.DESC,"dateTime");
        List<String> tags;
        tags = Arrays.stream(tag.split(";")).map((String x)->x.trim().toLowerCase()).collect(Collectors.toList());
        if (userService.isAdmin()){
            return postRepository.findByTags(tags,(long)tags.size(),pageable);
        }
        return postRepository.findByTagsAndNotHidden(tags,(long)tags.size(),pageable);
    }

    @Override
    public Post getPost(Long postId) {
        return postRepository.getOne(postId);
    }

    @Override
    public void setPostVisibility(Long postId, boolean hide) {
        Post post=getPost(postId);
        if (post==null){
            LOGGER.info("Error! PostId not found."+postId);
        }else {
            post.setHidden(hide);
            postRepository.saveAndFlush(post);
            LOGGER.info("Success! PostId haven been {}" + hide);
        }
    }

    @Override
    public void vote(Long postId, boolean like) throws AlreadyVotedException {
        Post post=postRepository.findOne(postId);
        if (post==null){
            LOGGER.info("Error! PostId not found: {}"+postId);
            return;
        }
        PostRating postRating=postRatingRepository.findByUserRating(postId,userService.currentUser().getId());
        if (postRating!=null){
            throw new AlreadyVotedException("You voted for the post");
        }

        postRating=new PostRating();
        postRating.setPost(post);
        postRating.setUser(userService.currentUser());
        postRating.setValue(like?Rating.LIKE_VALUE:Rating.DISLIKE_VALUE);
        postRatingRepository.saveAndFlush(postRating);
        LOGGER.info("Success! UserId {} haven been voted for postId {}"+userService.currentUser().getId(),+postId);
    }

    private Post fillPost(Post post, PostEditDto postEditDto){

        int cutInd = postEditDto.getText().indexOf(Post.shortPartSeparator());
        if (cutInd>0){
            String shortText = postEditDto.getText().substring(0,cutInd);
            List<String> links= MarkdownConverter.extractLinks(postEditDto.getText());
            if (links.size()>0){
                shortText+="\n"+links.stream().collect(Collectors.joining("\n"));
            }
            post.setShortTextPart(shortText);
        }else{
            post.setShortTextPart(null);
        }
        post.getTags().clear();
        String[] tags= Arrays.stream(postEditDto.getTags().split(",")).map(String::trim).toArray(String[]::new);
        for (String tagStr: tags){
            Tag tag=tagRepository.findByName(tagStr);
            if (tag==null){
                tag=new Tag();
                tag.setName(tagStr);
            }
            post.getTags().add(tag);
        }
        post.setFullPostText(postEditDto.getText());
        post.setDateTime(LocalDateTime.now());
        post.setTitle(postEditDto.getTitle());
        post.setId(postEditDto.getId());

        return post;
    }

    private PostEditDto convertToPostEditDto(Post post,PostEditDto postEditDto){
        postEditDto.setId(post.getId());
        postEditDto.setText(post.getFullPostText());
        postEditDto.setTitle(post.getTitle());
        postEditDto.setTags(post.getTags().stream().map(Tag::getName).collect(Collectors.joining("; ")));
        return postEditDto;
    }
}
