package com.mycompany.dbcooper.blog.Service;

import com.mycompany.dbcooper.blog.Model.Role;

import java.util.List;

public interface RoleService {
    List<Role> GetAllRole();
    Role findByName(String roleName);
}
