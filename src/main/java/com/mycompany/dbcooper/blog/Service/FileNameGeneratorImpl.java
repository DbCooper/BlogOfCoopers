package com.mycompany.dbcooper.blog.Service;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service("fileNameGenerator")
public class FileNameGeneratorImpl implements FileNameGenerator{
    @Override
    public String getFileName(String filename, String prefix) {
        return prefix+"_"+ RandomStringUtils.random(20,true,true)+"_"+System.currentTimeMillis();
    }
}
