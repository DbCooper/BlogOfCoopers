package com.mycompany.dbcooper.blog.Service;

public class UploadAvatarInfo {
    public final String bigImageLink;

    public final String smallImageLink;

    public UploadAvatarInfo(String bigImageLink, String smallImageLink) {
        this.bigImageLink = bigImageLink;
        this.smallImageLink = smallImageLink;
    }
}
