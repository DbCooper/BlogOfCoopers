package com.mycompany.dbcooper.blog.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ExceptionController {
    @RequestMapping(value = "/404")
    public String notFoundErrorHandler(HttpServletRequest request, Exception e) {
        return "404";
    }
    @RequestMapping(value = "/error",method = RequestMethod.GET)
    public String defaultErrorHandler(HttpServletRequest request, Exception e) {
        return "error";
    }
}
