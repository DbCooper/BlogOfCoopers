package com.mycompany.dbcooper.blog.Controller;

import com.mycompany.dbcooper.blog.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class LoginController {

    private static final Logger logger=LoggerFactory.getLogger(LoginController.class);
    
    @Autowired
    private UserService userService;

    @Autowired
    private PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

    @RequestMapping(value = "login",method = RequestMethod.GET)
    public String login(HttpServletRequest request, HttpSession session){
        if (userService.isAuthenticated()){
            String refer=request.getHeader("referer");
            session.setAttribute("refer",refer);
            return "login";
        }
        return "redirect:/";
    }
    @RequestMapping(value = "/login_success", method= RequestMethod.GET)
    public String loginSuccess(HttpServletRequest request,HttpSession session) {
        logger.info("Login Success");
        return "redirect:" + (StringUtils.isEmpty(session.getAttribute("refer"))?"/":session.getAttribute("refer"));
    }

    @RequestMapping(value = "/login_error", method= RequestMethod.GET)
    public String loginError(HttpServletRequest request, ModelMap model) {
        logger.info("Login failed");
        model.addAttribute("failed","Usenrame or ");
        return "login";
    }

    @RequestMapping(value = "logout",method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        if (authentication!=null){
            persistentTokenBasedRememberMeServices.logout(request,response,authentication);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/";
    }
}
