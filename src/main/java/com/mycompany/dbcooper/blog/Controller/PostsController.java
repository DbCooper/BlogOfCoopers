package com.mycompany.dbcooper.blog.Controller;

import com.mycompany.dbcooper.blog.Exception.AlreadyVotedException;
import com.mycompany.dbcooper.blog.Exception.ResourceNotFoundException;
import com.mycompany.dbcooper.blog.Model.Comment;
import com.mycompany.dbcooper.blog.Model.Post;
import com.mycompany.dbcooper.blog.Model.PostEditDto;
import com.mycompany.dbcooper.blog.Model.User;
import com.mycompany.dbcooper.blog.RabbitMQ.Consumer;
import com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver.NewPostNotification;
import com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver.Receiver;
import com.mycompany.dbcooper.blog.Repository.PostRatingRepository;
import com.mycompany.dbcooper.blog.Service.PostService;
import com.mycompany.dbcooper.blog.Service.UserService;
import com.mycompany.dbcooper.blog.Util.JsonUtils;
import com.mycompany.dbcooper.blog.Validator.CommonRabbitMQ;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/posts")
public class PostsController {
    private final Logger logger= LoggerFactory.getLogger(PostsController.class);
    @Autowired
    private PostService postService;

    @Autowired
    private PostRatingRepository postRatingRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    
    @Autowired
    private SimpMessageSendingOperations messageSendingOperations;
    
    @Autowired
    private Consumer consumer;

    @PostConstruct
    public void InitQueue() throws IOException{
        logger.info("Start create queue!");
        Receiver newPostNotification=new NewPostNotification(messageSendingOperations);
        
        if(!consumer.queueExists(CommonRabbitMQ.COMMON_QUEUE_NOTIFICATION, CommonRabbitMQ.COMMON_TOPIC,newPostNotification)){
            logger.info("Queue not exists:{}");
            consumer.createQueue(CommonRabbitMQ.COMMON_KEY_NOTIFICATION,CommonRabbitMQ.COMMON_QUEUE_NOTIFICATION,CommonRabbitMQ.COMMON_TOPIC, newPostNotification);
        }else{
            logger.info("Queue exists");
        }
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String getAllPost(@RequestParam(value = "page", defaultValue = "0") Integer pageNumber, ModelMap model){
        Page<Post> postPage = postService.getPostPage(5,pageNumber);

        User currentUser = userService.currentUser();
        model.addAttribute("postPage",postPage);
        if (userService.isAdmin()){
            model.addAttribute("userId",currentUser.getId());
        }
        logger.info("All Post:{}",postPage.getTotalElements());

        return "posts";
    }

    @RequestMapping(value = "/top",method = RequestMethod.GET,headers = "Accept=application/json",produces = "application/json;charset=UTF-8")
    public @ResponseBody String getTopPost(){
        List<Post> posts=postService.getTopPost();
        logger.info("Top post",posts.size());
        return "["+posts.stream().map(this::postToJson).collect(Collectors.joining(", \n"))+"]";
    }

    @RequestMapping(value = "/lasted",method = RequestMethod.GET,headers = "Accept=application/json",produces = "application/json;charset=UTF-8")
    public @ResponseBody String getLastedPost(){
        List<Post> posts=postService.getPostList(10,0);
        logger.info("Lasted post",posts.size());
        return "["+posts.stream().map(this::postToJson).collect(Collectors.joining(", \n"))+"]";
    }

    @RequestMapping(method = RequestMethod.GET,params = {"tagged"})
    public String seachByTag(@RequestParam("tagged") String tagged,@RequestParam(value = "page", defaultValue = "0") Integer pageNumber,
                              ModelMap model, HttpServletRequest request){
        logger.info("Search by tag: {}",tagged);
        if (StringUtils.isEmpty(tagged)) {
            return "redirect:/posts";
        }
        Page<Post> posts=postService.findByTags(tagged, 5, pageNumber);
        model.addAttribute("postPage", posts);

        model.addAttribute("searchTags", tagged);

        String query = "tagged=" + request.getParameter("tagged");
        model.addAttribute("searchQuery", query);

        User currentUser = userService.currentUser();
        if (currentUser != null)
            model.addAttribute("userId", currentUser.getId());
        logger.info("Size posts: {}",posts.getTotalElements());
        return "posts";
    }

    @RequestMapping(value = "/{postId}",method = RequestMethod.GET)
    public String getPostById(@PathVariable("postId") Long postId, ModelMap model){
        logger.info("Fetch post: {}",postId);
        Post post=postService.getPost(postId);
        if(post==null || post.isHidden() && !userService.isAdmin()){
            throw new ResourceAccessException("Not found!!");
        }
        model.addAttribute("post", post);
        if(!userService.isAuthenticated()){
            model.addAttribute("comment",new Comment());
        }
        
        User currentUser = userService.currentUser();
        if (currentUser != null)
            model.addAttribute("userId", currentUser.getId());
        logger.info("Info post: {}",post.toString());
        return "readpost";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{postId}/hide",method = RequestMethod.POST)
    public @ResponseBody String hidePost(@PathVariable("postId") Long postId){
        logger.info("Hide post id: {}",postId);
        try {
            postService.setPostVisibility(postId, true);
            logger.info("{} hide success",postId);
            return "ok";
        }catch (Exception ex){
            logger.info("Exception {}",ex.getMessage());
            return ex.getMessage();
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{postId}/unhide",method = RequestMethod.POST)
    public @ResponseBody String unhidePost(@PathVariable("postId") Long postId){
        logger.info("Hide post id: {}",postId);
        try{
            postService.setPostVisibility(postId,false);
            logger.info("{} unhide success",postId);
            return "ok";
        }catch (Exception ex){
            logger.info("Exception : {}",ex.getMessage());
            return ex.getMessage();
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{postId}/delete",method = RequestMethod.POST)
    public @ResponseBody String deletePost(@PathVariable("postId") Long postId){
        logger.info("Delete post :{}",postId);
        try{
            postService.deletePost(postId);
            logger.info("Delete success: {}",postId);
            return "ok";
        }catch (Exception ex){
            logger.info("Delete failed: {}",ex.getMessage());
            return ex.getMessage();
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public String getCreatePost(ModelMap model){
        PostEditDto post=new PostEditDto();
        model.addAttribute("post",post);
        model.addAttribute("edit",false);
        return "editpost";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public String createPost(ModelMap modelMap, @ModelAttribute("post") PostEditDto post, BindingResult result){
        if (result.hasErrors()){
            modelMap.addAttribute("edit", false);

            return "editpost";
        }
        Post newpost=postService.createPost(post);
        logger.info("Post:{}",newpost.getTitle());
        try{
            logger.info("Start send data to queue.");
            String notificationJson=postToJson(newpost);
            Message toQueue= MessageBuilder.withBody(notificationJson.getBytes())
				.andProperties(MessagePropertiesBuilder.newInstance().setContentType("application/json")
				.build())
                                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                                .setDeliveryTag(new Random().nextLong())
                                .build();
            rabbitTemplate.convertAndSend(CommonRabbitMQ.COMMON_TOPIC,CommonRabbitMQ.COMMON_KEY_NOTIFICATION,toQueue);
            logger.info("Success send data to queue.");
        }catch (AmqpException ex){
            logger.info("Error! "+ex.getMessage());
        }
        logger.info("Created post success");
        return "redirect:/posts";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{postId}/edit",method = RequestMethod.GET)
    public String getEditPost(ModelMap modelMap,@PathVariable("postId") Long postId){
        PostEditDto post=postService.editPost(postId);
        if (post==null){
            throw new ResourceNotFoundException();
        }
        modelMap.addAttribute("post",post);
        modelMap.addAttribute("edit",true);
        return "editpost";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/{postId}/edit",method = RequestMethod.POST)
    public String updatePost(ModelMap modelMap, @ModelAttribute("post") PostEditDto post, BindingResult result,@PathVariable("postId") Long postId){
        post.setId(postId);
        if (result.hasErrors()){
            modelMap.addAttribute("edit", true);
            return "editpost";
        }
        postService.updatePost(post);
        logger.info("Update post success");
        return "redirect:/posts";
    }
    
    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/{postId}/like",method = RequestMethod.POST)
    public @ResponseBody String likePost(@PathVariable("postId") Long postId){
        try{
            postService.vote(postId,true);
            logger.info("Like post success: {}",postId);
            return "ok";
        } catch (AlreadyVotedException e) {
            logger.info("Like post failed: {}",e.getMessage());
            return ""+e.getMessage();
        }
    }
    
    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/{postId}/dislike",method = RequestMethod.POST)
    public @ResponseBody String unlikePost(@PathVariable("postId") Long postId){
        try{
            postService.vote(postId,false);
            logger.info("Unlike post success: {}",postId);
            return "ok";
        } catch (AlreadyVotedException e) {
            logger.info("Unlike post failed: {}",e.getMessage());
            return ""+e.getMessage();
        }
    }
    private String postToJson(Post post){
        return "{"+ JsonUtils.toJsonField("id",post.getId().toString())+","+JsonUtils.toJsonField("title",post.getTitle())+"}";
    }
}
