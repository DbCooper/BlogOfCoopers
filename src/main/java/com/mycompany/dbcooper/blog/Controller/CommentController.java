package com.mycompany.dbcooper.blog.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dbcooper.blog.Exception.ActionExpiredException;
import com.mycompany.dbcooper.blog.Exception.AlreadyVotedException;
import com.mycompany.dbcooper.blog.Exception.AuthException;
import com.mycompany.dbcooper.blog.Exception.ForbiddenException;
import com.mycompany.dbcooper.blog.Exception.ResourceNotFoundException;
import com.mycompany.dbcooper.blog.Model.Comment;
import com.mycompany.dbcooper.blog.Model.Post;
import com.mycompany.dbcooper.blog.Model.User;
import com.mycompany.dbcooper.blog.Service.CommentService;
import com.mycompany.dbcooper.blog.Service.PostService;
import com.mycompany.dbcooper.blog.Service.UserService;
import com.mycompany.dbcooper.blog.Util.JsonUtils;
import com.mycompany.dbcooper.blog.Util.RabbitSendToQueue;
import com.mycompany.dbcooper.blog.Validator.UserRabbitMQ;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class CommentController {
        private final Logger logger =LoggerFactory.getLogger(CommentController.class);

    @Autowired
    private CommentService commentService;

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private RabbitSendToQueue sendToQueue;
    
    @Autowired
    private SimpMessageSendingOperations messageSendingOperations;
    
    @RequestMapping(value = "/posts/{postId}/comments",method = RequestMethod.GET)
    public String getComments(@PathVariable("postId") Long postId, ModelMap modelMap){
        Post post=postService.getPost(postId);
        if (post==null){
            throw new ResourceNotFoundException();
        }
        if (post.isHidden() && !userService.isAdmin()){
            throw new ResourceNotFoundException();
        }
        modelMap.addAttribute("comments",post.getComments());
        modelMap.addAttribute("post",post);
        return "fragment/comments :: commentList";
    }
    
    @MessageMapping("/wss/vote")
    public void userVoteComment(@Payload Map<String,String> dataVote){
         String typeVote= dataVote.get("typeVote");
         Long idComment = Long.parseLong(dataVote.get("idComment"));
         Long idPost = Long.parseLong(dataVote.get("idPost"));
         String action=dataVote.get("action");
         boolean voted=typeVote.equals("like")?true:false;
         
         try{
             commentService.vote(idComment, voted);
             Comment comment=commentService.getComment(idComment);
             
             Long ownerId= comment.getUser().getId();
             String username=comment.getUser().getUsername();
             String dataQueue=convertToSend(idPost, idComment, typeVote,username,action);
            
             sendToQueue.sendToQueue(UserRabbitMQ.PREFIX_EXCHANGE+ownerId,UserRabbitMQ.PREFIX_KEY+"notifycation_"+ownerId, dataQueue);
             logger.info("Send To Queue Success: ",dataQueue);
             String postListent="/wss/posts/"+idPost;
             messageSendingOperations.convertAndSend(postListent, convertToSend(idPost, idComment, typeVote, userService.currentUser().getUsername(),action));
        }catch(AlreadyVotedException ex){
             logger.info("Error load: ",ex.getMessage());
        }catch(ForbiddenException ex){
            logger.info("Error {} ",ex.getMessage());
        }         
    }
    
    @MessageMapping("/wss/comment")
    public void createComment(@Payload Map<String,String> dataComment){
        logger.info("Data commnet: {}",dataComment);
        try{
            ObjectMapper mapper = new ObjectMapper();
            Comment comment=new Comment();
            String commentContent=dataComment.get("commentContent");
            Long postId=Long.parseLong(dataComment.get("idPost"));
            Long parentId=Long.parseLong(dataComment.get("parentComment"));
            String action=dataComment.get("action");
            comment.setCommentText(commentContent);
            Comment commentNew = commentService.saveNewComment(comment, postId, parentId==0?null:parentId);

            Hibernate.initialize(commentNew.getPost().getComments());
            List<Comment> comments=commentNew.getPost().getComments();
            List<User> users=comments.stream().filter(x->x.isDeleted()==false).map(x->x.getUser()).distinct().collect(Collectors.toList());
            String listId=users.stream().map(x->x.getId().toString()).collect(Collectors.joining(" | "));
            logger.info("String {}",listId);
            users.forEach(x->sendToQueue.sendToQueue(UserRabbitMQ.PREFIX_EXCHANGE+x.getId(),
                                                             UserRabbitMQ.PREFIX_KEY+"notifycation_"+x.getId(),
                                                              convertToSend(postId, comment.getId(), "", x.getUsername(), action)));

            String postListent="/wss/posts/comment/"+postId;
            messageSendingOperations.convertAndSend(postListent,parseCommentToJson(commentNew));
            logger.info("wss: "+postListent+"data:{}",parseCommentToJson(commentNew));
        }catch(NumberFormatException ex){
            logger.info("Exception {}",ex.getMessage());
            ex.printStackTrace();
        }
    }
    @MessageMapping("/wss/comment/delete")
    public void deleteComment(@Payload Map<String,String> dataPay){
        String postId=dataPay.get("postId");
        String commentId=dataPay.get("commentId");
        String postListent="/wss/posts/comment/delete/"+postId;
        logger.info("Data Payload:{}",dataPay);
        try{
           commentService.deleteComment(Long.parseLong(commentId));
           messageSendingOperations.convertAndSend(postListent, "ok-"+commentId);
           logger.info("Success delete: {}",commentId);
        }catch(ActionExpiredException | AuthException ae){
            logger.info("Error: {}",ae.getMessage());
            messageSendingOperations.convertAndSend(postListent,"error-"+ ae.getMessage());
            logger.info("Send to socket success");
        }
        
    } 
    
    public String convertToSend(Long postId,Long commnetId, String vote,String username,String action){
        return "{"+JsonUtils.toJsonField("postId", postId.toString())+",\n"+JsonUtils.toJsonField("commentId", commnetId.toString())+",\n"+JsonUtils.toJsonField("vote", vote)+",\n"+JsonUtils.toJsonField("username", username)+",\n"+JsonUtils.toJsonField("action", action)+"}";
    }
    
    public String parseCommentToJson(Comment comment){
        String dateTime=convertDateTime(comment.getDateTime().toString());
        return "{"+JsonUtils.toJsonField("postId", comment.getPost().getId().toString())+",\n"
        			+JsonUtils.toJsonField("commentId",comment.getId().toString())+",\n"
        			+JsonUtils.toJsonField("contentComment", comment.getCommentTextHtml())+",\n"
        			+JsonUtils.toJsonField("parentId", comment.getParentComment()==null?"0":comment.getParentComment().getId().toString())+",\n"
        			+JsonUtils.toJsonField("userComment",comment.getUser().getUsername())+",\n"
                    +JsonUtils.toJsonField("dateTime", dateTime)+",\n"
                    +JsonUtils.toJsonField("userImage", comment.getUser().getSmallAvatarLink())

        			+"}";
    }
    
    private String convertDateTime(String datetime){
     
        LocalDateTime now = LocalDateTime.parse(datetime);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:mm");

        String formatDateTime = now.format(formatter);
        return formatDateTime;
    }
    
}
