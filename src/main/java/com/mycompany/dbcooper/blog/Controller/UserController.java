package com.mycompany.dbcooper.blog.Controller;

import com.mycompany.dbcooper.blog.Exception.AuthException;
import com.mycompany.dbcooper.blog.Exception.ResourceNotFoundException;
import com.mycompany.dbcooper.blog.Exception.UnsupportedFormatException;
import com.mycompany.dbcooper.blog.Model.User;
import com.mycompany.dbcooper.blog.Repository.RoleRepository;
import com.mycompany.dbcooper.blog.Service.AvatarService;
import com.mycompany.dbcooper.blog.Service.RoleService;
import com.mycompany.dbcooper.blog.Service.UploadAvatarInfo;
import com.mycompany.dbcooper.blog.Service.UserService;
import com.mycompany.dbcooper.blog.Util.JsonUtils;
import com.mycompany.dbcooper.blog.Validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.print.Pageable;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {
    private final Logger logger= LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    private final UserValidator userValidator;

    private final AvatarService avatarService;

    @Autowired
    private final RoleService roleService;

    private final HttpSession session;
    @Autowired
    public UserController(RoleService roleService,UserService userService, UserValidator userValidator, AvatarService avatarService,HttpSession session) {
        this.userService = userService;
        this.userValidator = userValidator;
        this.avatarService = avatarService;
        this.session=session;
        this.roleService=roleService;
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String index(){
        logger.info("Roles: {}",SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        return "index";
    }
    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public String register(ModelMap model, HttpSession session, HttpServletRequest request){
        if (userService.isAuthenticated()){
            User user=new User();
            model.addAttribute("user",user);
            String refer=request.getHeader("referer");
            session.setAttribute("refer",refer);
            return "register";
        }
        return "redirect:/";
    }

    @RequestMapping (value = "/register",method = RequestMethod.POST)
    public String register(ModelMap model, HttpServletRequest request, HttpSession session, @Validated({User.CreateValidationGroup.class}) @ModelAttribute(value = "user") User user, BindingResult result){
        user.setUsername(org.springframework.util.StringUtils.trimWhitespace(user.getUsername()));
        user.setEmail(StringUtils.trimWhitespace(user.getEmail()));

        userValidator.validate(user,result);

        if (result.hasErrors()){
            return "register";
        }

        userService.register(user);
        return "redirect:login";
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/personal/{username}", method = RequestMethod.GET)
    public String showProfile(@PathVariable("username") String username, ModelMap model,HttpSession session) {
        User user = userService.findByUsername(username);
        if (user == null)
            throw new ResourceNotFoundException();
        model.addAttribute("user", user);
        return "profile";
    }


    @PreAuthorize("hasRole('USER')")
    @CrossOrigin
    @RequestMapping(value = "/upload_avatar", method = RequestMethod.POST)
    public @ResponseBody String uploadAvatar(@RequestParam("uploadImage") MultipartFile file) throws IOException {
       logger.info("Start upload");
        try {
            UploadAvatarInfo result = avatarService.uploadAvavatar(file);

            userService.changeAvatar(result);
            updateUserSession();
            return makeAvatarUploadResponse("ok", result);
        } catch (UnsupportedFormatException e) {
            return makeAvatarUploadResponse("invalid_format", null);
        }
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/change_password",method = RequestMethod.POST)
    public @ResponseBody String changePassword(@RequestBody Map<String,String> params){

        logger.info("Post demo: "+params);
        String oldPass=params.get("oldPass");
        String newPass=params.get("newPass");
        try{
            userService.changePassword(oldPass,newPass);
            updateUserSession();
            return makeChangeProfileResponse("ok","Password has been changed");
        } catch (AuthException e) {
            return makeChangeProfileResponse("error","Password Not Match Current");
        }
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/change_email",method = RequestMethod.POST)
    public @ResponseBody String changeEmail(@RequestBody Map<String,String> params){
        String newEmail=params.get("newEmail");
        String oldPass=params.get("oldPass");
        try{
            userService.changeEmail(newEmail,oldPass);
            updateUserSession();
            return makeChangeProfileResponse("ok",newEmail);
        } catch (AuthException e) {
            return makeChangeProfileResponse("error","Password Not Match Current");
        }
    }


    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/remove_avatar",method = RequestMethod.POST)
    public @ResponseBody String removeAvatar() throws IOException{
        userService.removeAvatar();
        updateUserSession();
        return "ok";
    }

    @RequestMapping(value = "/upload_profile",method = RequestMethod.POST)
    public @ResponseBody String changeProfile(@RequestBody Map<String,String> request)
    {
        logger.info("Data request body: {}",request);
        try{
            String about=request.get("about");
            String linkWebsite=request.get("linkWebsite");
            User user=userService.currentUser();
            user.setAboutText(about);
            user.setWebsiteLink(linkWebsite);
            userService.changeProfileInfo(user);
            updateUserSession();
            return makeChangeProfileResponse("ok", "Update profile success");
        }catch(Exception ex){
            return makeChangeProfileResponse("error", ex.getMessage());
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/admin/usermanagement",method = RequestMethod.GET)
    public String GetAllUser(@RequestParam(defaultValue = "0",name = "page") Integer pageNumber,ModelMap modelMap,@RequestParam(value = "username",defaultValue = "") String username){
        Page<User> allUser=userService.findAllUserByUsername(username,5,pageNumber);
        modelMap.addAttribute("allUser",allUser);

        modelMap.addAttribute("username",username);
        return "usermanagement";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/admin/actionroles",method = RequestMethod.POST)
    public @ResponseBody String ActionRole(@RequestBody Map<String,String> dataRequest){
        String action = dataRequest.get("action");
        String userId = dataRequest.get("userId");
        logger.info("action: {}, userId: {}",action,userId);
        try{
            switch (action){
                case "ADD_ADMIN":
                    userService.addRole("ROLE_ADMIN",Long.parseLong(userId));
                    break;
                case "REMOVE_ADMIN":
                    userService.removeRole("ROLE_ADMIN",Long.parseLong(userId));
                    break;

                    default:
                        return JsonUtils.toJsonField("status","Invalid role name");

            }
            return JsonUtils.toJsonField("status","success");

        }catch (AuthException ex){
            return JsonUtils.toJsonField("status",ex.getMessage());
        }
    }

    private String makeAvatarUploadResponse(String status, UploadAvatarInfo uploadedAvatarInfo) {
        return "{" + JsonUtils.toJsonField("status", status) +
                (uploadedAvatarInfo == null ? "" : (", " + JsonUtils.toJsonField("link", uploadedAvatarInfo.bigImageLink))) +
                "}";
    }
    private String makeChangeProfileResponse(String status, String message){
        return "{"+JsonUtils.toJsonField("status",status)+","+JsonUtils.toJsonField("message",message)+"}";
    }
    private void updateUserSession(){
        session.setAttribute("userCurrent", userService.findByUsername(userService.currentUser().getUsername()));
    }
    //RedirectAttributes: truyền dữ liệu giữa các controller
}
