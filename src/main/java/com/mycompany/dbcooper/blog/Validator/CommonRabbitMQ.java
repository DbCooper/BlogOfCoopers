/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dbcooper.blog.Validator;

/**
 *
 * @author DBCooper
 */
public class CommonRabbitMQ {
    public static final String COMMON_QUEUE_NOTIFICATION="common_notification";
    public static final String COMMON_TOPIC="common_topic";
    public static final String COMMON_KEY_NOTIFICATION="key.admin.notification";
}
