package com.mycompany.dbcooper.blog.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class BlogConfiguration extends WebMvcConfigurerAdapter {
    @Autowired
    private SimpMessageSendingOperations template;
}
