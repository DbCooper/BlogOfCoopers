package com.mycompany.dbcooper.blog.Configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;


@Configuration
@EnableWebSocketMessageBroker
public class WebSocketBrokerConfiguration extends AbstractWebSocketMessageBrokerConfigurer  {
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/wss/notification", "/wss/status","/wss/messages/","/wss/posts/");
        config.setApplicationDestinationPrefixes("/blogcooper-wss");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/wss/sendNotification", "/wss/comment","/wss/vote","/wss/comment/delete");
        registry.addEndpoint("/wss").withSockJS();
    }

}

    
