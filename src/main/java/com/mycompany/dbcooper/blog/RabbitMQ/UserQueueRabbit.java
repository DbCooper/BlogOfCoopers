/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dbcooper.blog.RabbitMQ;

import com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver.Receiver;
import com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver.UserNotifycation;
import com.mycompany.dbcooper.blog.Validator.UserRabbitMQ;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

/**
 *
 * @author DBCooper
 */
@Component
public class UserQueueRabbit {
    @Autowired
    private Consumer consumer;
    
    @Autowired
    private SimpMessageSendingOperations messageSendingOperations;
    public void createNotifycationQueueUser(Long userId) throws IOException{
        Receiver receiver=new UserNotifycation(messageSendingOperations);
        boolean checkqueue=consumer.queueExists(UserRabbitMQ.PREFIX_QUEUE+"notifycation_"+userId, UserRabbitMQ.PREFIX_EXCHANGE+userId, receiver);
        if(!checkqueue){
            consumer.createQueue(UserRabbitMQ.PREFIX_KEY+"notifycation_"+userId, UserRabbitMQ.PREFIX_QUEUE+"notifycation_"+userId, UserRabbitMQ.PREFIX_EXCHANGE+userId, receiver);
        }
    }
    
    //most of queue
}
