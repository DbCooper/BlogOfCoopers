/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.dbcooper.blog.Service.UserService;
import com.rabbitmq.client.Channel;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

/**
 *
 * @author DBCooper
 */
public class UserNotifycation extends MessageListenerAdapter implements Receiver{
    private final Logger logger=LoggerFactory.getLogger(UserNotifycation.class);

    @Autowired
    private UserService userService;

    private SimpMessageSendingOperations messagingTemplate;

    public UserNotifycation(SimpMessageSendingOperations messagingTemplate){
        this.messagingTemplate=messagingTemplate;
    }
    @Override
    public void onMessage(Message exchange,Channel channel) throws IOException {
        ObjectMapper mapper=new ObjectMapper();
        logger.info("Message: {}",new String(exchange.getBody()));
        Map<String,String> valueQueue=new HashMap<>();
        valueQueue=mapper.readValue(new String(exchange.getBody()), new TypeReference<Map<String, String>>(){});
        logger.info("User name: {}",valueQueue.get("username"));
        channel.basicAck(exchange.getMessageProperties().getDeliveryTag(), false);
        messagingTemplate.convertAndSendToUser(valueQueue.get("username"),"/wss/notification",new String(exchange.getBody()));

    }
}
