package com.mycompany.dbcooper.blog.RabbitMQ;

import com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver.Receiver;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class Consumer {
    private final Logger logger =LoggerFactory.getLogger(Consumer.class);
    @Autowired
    private ConnectionFactory connectionFactory;
    @Autowired
    private MessageConverter messageConverter;

    public void createQueue( String routingKey, String queueName,String topicName,Receiver receiver){
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        Map<String, Object> modequeue = new HashMap<String, Object>();
        modequeue.put("x-queue-mode", "lazy");
        Queue queue = new Queue(queueName,false,false,false,modequeue);
        admin.declareQueue(queue);
        TopicExchange exchange = new TopicExchange(topicName);
        admin.declareExchange(exchange);
        admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(routingKey));

        // set up the listener and container
        SimpleMessageListenerContainer container =
                new SimpleMessageListenerContainer(connectionFactory);
        MessageListenerAdapter adapter=new MessageListenerAdapter(receiver);
        container.setMessageConverter(messageConverter);
        container.setMessageListener(adapter);
        container.setQueueNames(queueName);
        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        container.start();

    }
    
    public boolean queueExists(String queueName,String topicName,Receiver receiver) throws IOException{
        Connection connection=connectionFactory.createConnection();
        Channel channel=connection.createChannel(true);
        //channel.exchangeDeclare(topicName,"topic");
        try{
            AMQP.Queue.DeclareOk result= channel.queueDeclarePassive(queueName);
            logger.info("data queue: {}", channel.basicGet(queueName, false));
            // set up the listener and container
            SimpleMessageListenerContainer container =
                    new SimpleMessageListenerContainer(connectionFactory);
            MessageListenerAdapter adapter=new MessageListenerAdapter(receiver);
            container.setMessageConverter(messageConverter);
            container.setMessageListener(adapter);
            container.setQueueNames(queueName);

            container.start();
            return result != null;
        }catch(IOException ex){
            return ex.getCause().getMessage().contains("RESOURCE_LOCKED");
        }
    }
}
