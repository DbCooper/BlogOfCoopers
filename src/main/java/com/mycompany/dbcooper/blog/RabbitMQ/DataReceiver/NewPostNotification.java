/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dbcooper.blog.RabbitMQ.DataReceiver;

import com.mycompany.dbcooper.blog.Service.UserService;
import com.rabbitmq.client.Channel;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

/**
 *
 * @author DBCooper
 */
public class NewPostNotification extends MessageListenerAdapter implements Receiver{

    private final Logger logger=LoggerFactory.getLogger(NewPostNotification.class);

    @Autowired
    private UserService userService;

    private SimpMessageSendingOperations messagingTemplate;

    public NewPostNotification(SimpMessageSendingOperations messagingTemplate){
        this.messagingTemplate=messagingTemplate;
    }
    @Override
    public void onMessage(Message exchange,Channel channel) throws IOException {
        
        logger.info("Message: {}",new String(exchange.getBody()));
        channel.basicAck(exchange.getMessageProperties().getDeliveryTag(), false);
        messagingTemplate.convertAndSend("/wss/notification",new String(exchange.getBody()));
    }

    
}
