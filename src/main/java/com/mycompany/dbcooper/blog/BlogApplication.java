package com.mycompany.dbcooper.blog;

import com.mycompany.dbcooper.blog.Configuration.BlogConfiguration;
import com.mycompany.dbcooper.blog.Configuration.JpaConfiguration;
import com.mycompany.dbcooper.blog.Configuration.RabbitConfiguration;
import com.mycompany.dbcooper.blog.Configuration.WebSocketBrokerConfiguration;
import com.mycompany.dbcooper.blog.Security.WebSecurityConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import({JpaConfiguration.class, WebSecurityConfiguration.class,BlogConfiguration.class,WebSecurityConfiguration.class,WebSocketBrokerConfiguration.class,RabbitConfiguration.class})
@SpringBootApplication(scanBasePackages={"com.mycompany.dbcooper.blog"})
public class BlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}
}
