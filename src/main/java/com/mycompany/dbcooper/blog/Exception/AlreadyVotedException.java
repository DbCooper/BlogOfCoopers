package com.mycompany.dbcooper.blog.Exception;

public class AlreadyVotedException extends Exception{
    public AlreadyVotedException(String message){
        super(message);
    }
}
