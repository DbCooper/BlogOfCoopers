package com.mycompany.dbcooper.blog.Exception;

public class AuthException extends Exception {

    public AuthException(String message){
        super(message);
    }
}
