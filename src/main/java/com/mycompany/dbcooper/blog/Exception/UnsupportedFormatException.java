package com.mycompany.dbcooper.blog.Exception;

public class UnsupportedFormatException extends Exception {
    public UnsupportedFormatException(String message){
        super(message);
    }
}
