package com.mycompany.dbcooper.blog.Exception;

public class ActionExpiredException extends Exception {
    public ActionExpiredException(String message){
        super(message);
    }
}
