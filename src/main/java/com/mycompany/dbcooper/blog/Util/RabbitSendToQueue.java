/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dbcooper.blog.Util;

import com.mycompany.dbcooper.blog.Validator.CommonRabbitMQ;
import java.util.Random;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author DBCooper
 */
@Component
public class RabbitSendToQueue {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    public void sendToQueue(String exchange,String keyqueue, String data){
        Message toQueue= MessageBuilder.withBody(data.getBytes())
				.andProperties(MessagePropertiesBuilder.newInstance().setContentType("application/json")
				.build())
                                .setDeliveryMode(MessageDeliveryMode.PERSISTENT)
                                .setDeliveryTag(new Random().nextLong())
                                .build();
            rabbitTemplate.convertAndSend(exchange,keyqueue,toQueue);
    }
}
