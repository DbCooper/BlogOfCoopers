$(document).ready(function() {
    var converter = Markdown.getSanitizingConverter();

    var editor = new Markdown.Editor(converter);

    editor.run();

    $("#uploadImage").on('change', function () {
        //Get count of selected files
        var countFiles = $(this)[0].files.length;
        var imgPath = $(this)[0].value;
        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
        var image_holder = $("#image-holder");
        image_holder.empty();
        if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {
                //loop for each file selected for uploaded.
                for (var i = 0; i < countFiles; i++) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("<img />", {
                            "src": e.target.result,
                            "class": "img-thumbnail"
                        }).appendTo(image_holder);
                    }
                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[i]);
                }
            } else {
                alertDialog("Format Image","This browser does not support FileReader.","warning")
            }
        } else {
            alert("");
            alertDialog("File not Choose","Pls select only images","warning")
        }
    });


     $('#avatarFileUploadInput').click(function(e) {
        e.preventDefault();
        //Disable submit button
        $(this).prop('disabled',true);

        var formData = new FormData();
        formData.append("uploadImage",$('#uploadImage').get(0).files[0]);
        // Ajax call for file uploaling
        var ajaxReq = $.ajax({
            url : window.avatarUploadUrl,
            type: 'POST',
            data : formData,
            cache: false,
            contentType: false,
            processData: false,
            enctype: 'multipart/form-data',
            timeout: 600000,
            xhr: function(){
                //Get XmlHttpRequest object
                var xhr = $.ajaxSettings.xhr() ;

                //Set onprogress event handler
                xhr.upload.onprogress = function(event){
                    var perc = Math.round((event.loaded / event.total) * 100);
                    $('#progressBar').text(perc + '%');
                    $('#progressBar').css('width',perc + '%');
                };
                return xhr ;
            },
            beforeSend: function() {
                //Reset alert message and progress bar
                $('#alertMsg').text('');
                $('#progressBar').text('');

            }
        });

        // Called on success of file upload
        ajaxReq.done(function(msg) {
            alertDialog("Change Avatar","You have successfully changed your avatar\n","success");
            $('input[type=file]').val('');
            var object=JSON.parse(msg);
            console.log(object.link);
            $(".user-avatar").attr("src","/blog/uploads/avatars/"+object.link);
            $("#removeAvatar").show();

        });

        // Called on failure of file upload
        ajaxReq.fail(function(jqXHR) {
           console.log(jqXHR.responseText+'('+jqXHR.status+
                ' - '+jqXHR.statusText+')');
            alertDialog("Change Avatar","An error occurred during upload ! Try again.\n","error");
            $('#avatarFileUploadInput').prop('disabled',false);
            $('#progressBar').removeClass("progress-bar-success").addClass("progress-bar-danger");
        });
   
    });

   


   /* $('#avatarFileUploadInput').fileupload({
        url: window.avatarUploadUrl,
        dataType: "json",
        formData: [
            { name: "_csrf", value: token},
            { name: "_csrf_header", value: header}
        ],
        send: function (e, data,xhr) {

            pb.css('width', '0');
            pb.switchClass('progress-bar-danger', 'progress-bar-success', 0);
            pb.parent().show();

            avatarErrorLabel.hide();
            avatarSuccessLabel.hide();

            uploadBtn.addClass('disabled');
            removeBtn.addClass('disabled');
        },
        done: function (e, data) {
            if (data.result.status == 'ok') {
                avatarImg.attr('src', window.imgBaseUrl + data.result.link);

                removeBtn.show();

                avatarSuccessLabel.show();
            }
            else {
                pb.switchClass('progress-bar-success', 'progress-bar-danger');

                var errMsg = 'Failed to upload, ' + data.result.status;

                if (data.result.status == 'invalid_format') {
                    errMsg = 'Only JPG and PNG allowed.';
                }

                avatarErrorLabel.text(errMsg);
                avatarErrorLabel.show();
            }
        },
        fail: function (e, data) {
            pb.switchClass('progress-bar-success', 'progress-bar-danger');

            avatarErrorLabel.text('Failed to upload picture. Check that it is PNG or JPG and not exceeds 1 MB');
            avatarErrorLabel.show();
        },
        always: function (e, data) {
            uploadBtn.removeClass('disabled');
            removeBtn.removeClass('disabled');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            pb.css('width', progress + '%');
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');*/

});
