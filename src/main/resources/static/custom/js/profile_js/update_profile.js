$(document).ready(function (evt) {
    $("#actionChangePassword").click(function (evt) {
        evt.preventDefault();
        var oldPass=$("#inputPassword").val();
        var newPass=$("#inputNewPassword").val();
        var confirmPass=$("#inputConfirmPassword_1").val();
        if (newPass.length>7 && newPass==confirmPass) {
            $("#inputNewPassword").parent().removeClass("has-error has-feedback");
            $("#inputNewPassword").next().attr("style","display:none");
            $("#inputConfirmPassword_1").parent().removeClass("has-error has-feedback");
            $("#inputConfirmPassword_1").next().attr("style","display:none");
            $.ajax({
                url: window.changePassUrl,
                dataType: "json",
                contentType: "application/json",
                type: "POST",
                data: JSON.stringify({
                    oldPass: oldPass,
                    newPass: newPass
                }),
                success: function (data) {
                    $("#inputConfirmPassword_1").parent().removeClass("has-error has-feedback");
                    if (data.status == "ok") {
                        alertDialog("Change Password", data.message, "success");

                    } else {
                        alertDialog("Change Password", data.message, "error");

                    }

                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });
        }else if (newPass.length<7){
            $("#inputNewPassword").parent().addClass("has-error has-feedback");
            $("#inputNewPassword").next().attr("style","display:block");
            $("#inputNewPassword").next().text("Passwords are at least 7 characters long");
        }else{
            $("#inputConfirmPassword_1").parent().addClass("has-error has-feedback");
            $("#inputConfirmPassword_1").next().attr("style","display:block");
            $("#inputConfirmPassword_1").next().text("Passwords do not match.")

        }
    })

    $("#actionChangeEmail").click(function (evt) {
        evt.preventDefault();
        var  newEmail=$("#inputEmail").val();
        var oldPass =$("#inputConfirmPassword_2").val();

            $.ajax({
                url: window.changeEmailUrl,
                dataType: "json",
                contentType: "application/json",
                type: "POST",
                data: JSON.stringify({
                    oldPass: oldPass,
                    newEmail: newEmail
                }),
                success: function (data) {
                    $("#inputConfirmPassword_1").parent().removeClass("has-error has-feedback");
                    if (data.status == "ok") {
                        $("#inputEmail").val(data.message);
                        alertDialog("Change Password","Change email success", "success");
                    } else {
                        alertDialog("Change Password", data.message, "error");
                    }

                },
                error: function (err) {
                    console.log(err.responseText);
                }
            });

    })

    $("#removeAvatar").click(function (evt) {
        evt.preventDefault();
        var url =$(this).attr("data-href");
        $.ajax({
           url:url,
           type:'POST',
           success: function (success) {
               console.log(success);
               if (success=='ok'){
                   alertDialog("Remove Avatar","Remove Success","success");
                   $(".user-avatar").attr("src",window.rootAvatar);
                   $("#removeAvatar").hide();
               }else{
                   alertDialog("Remove Avatar","Remove Error","error");
               }
           },
            error: function (err) {
                console.log(err.responseText);
                alertDialog("Remove Avatar","Remove Failed","error");
            }
        });
    });

    $("#submitProfile").click(function (evt) {
        evt.preventDefault();
        var textAbout=$("#wmd-input").val();
        var links=$("#inputWebsiteLink").val();
        $.ajax({
            url: window.uploadProfileUrl,
            dataType: "json",
            type:"POST",
            contentType:"application/json",
            data: JSON.stringify({
                about:textAbout,
                linkWebsite:links
            }),
            success: function (success) {
                if(success.status=="ok"){
                    alertDialog("Update Profile",success.message,"success");
                }else{
                    alertDialog("Update Profile",success.message,"error");
                }
            },
            error: function (err) {
                console.log(err.responseText);
            }
        });
    });

})