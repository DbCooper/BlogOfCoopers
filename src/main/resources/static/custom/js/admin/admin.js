$(document).ready(function (evt) {
var postContainer=$("#pagePostsContent");
    postContainer.on('click',"a[data-action='hidePost']",function (evt) {
        evt.preventDefault();
        var urlAction=$(this).attr("data-href");
        var idPost=$(this).closest(".post").attr("data-post-id");
        var postTitle=$(this).closest(".post").attr("data-post-title");
        var btn=$(this);
        bootbox.confirm({
            message: 'Are you sure you want to hide post <b>' + postTitle + '</b> from other users?',
                buttons: {
                confirm: {
                    label: 'Yes',
                        className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                        className: 'btn-danger'
                }
            },
            callback: function (result) {
                 if (result) {
                     $.ajax({
                         url: urlAction,
                         type: 'POST',
                         success: function (success) {
                             if (success == "ok") {
                                 btn.closest(".post").find(".page-content").append('<div class="hidden-post"><span>Not visible for users</span></div>');
                                 btn.closest(".post-actions").find('a[data-action="deletePost"]').show();
                                 btn.attr("data-action", "unhidePost");
                                 var unhide_href = urlAction.replace("hide", "unhide");
                                 btn.attr("data-href", unhide_href);
                                 btn.html('<i class="fa fa-eye"></i><span>Unhide</span></a>');
                                 alertDialog("Hide Post", 'Hide post ' + postTitle + ' success', 'success');
                             } else {
                                 alertDialog("Hide Post", success, 'error');
                             }
                         },
                         error: function (err) {
                             console.log(err.responseText);
                             alertDialog("Hide Post", 'An error occurred is out of the process. Try again', 'error');

                         }
                     });
                 }
            }
        })
    })

    postContainer.on('click',"a[data-action='unhidePost']",function (evt) {
        evt.preventDefault();
        var urlAction=$(this).attr("data-href");
        var idPost=$(this).closest(".post").attr("data-post-id");
        var postTitle=$(this).closest(".post").attr("data-post-title");
        var btn=$(this);
        bootbox.confirm({
            message: 'Are you sure you want to unhide post <b>' + postTitle + '</b> from other users?',
                buttons: {
                confirm: {
                    label: 'Yes',
                        className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                        className: 'btn-danger'
                }
            },
            callback: function(result){
                if (result){
                     $.ajax({
                         url: urlAction,
                         type:'POST',
                         success: function (success) {
                             if (success=="ok"){
                                 btn.closest(".post").find(".page-content .hidden-post").remove();
                                 btn.closest(".post-actions").find('a[data-action="deletePost"]').hide();
                                 btn.attr("data-action","hidePost");
                                 var unhide_href=urlAction.replace("unhide","hide");
                                 btn.attr("data-href",unhide_href);
                                 btn.html('<i class="fa fa-eye"></i><span>Hide</span></a>');
                                 alertDialog("Unhide Post",'Unhide post '+postTitle+' success','success');
                             }else{
                                 alertDialog("Unhide Post",success,'error');
                             }
                         },
                         error: function (err) {
                             console.log(err.responseText);
                             alertDialog("Unhide Post",'An error occurred is out of the process. Try again','error');
                         }
                     });
                 }else{
                    console.log("Cancel!!");
                }
             }
        })
    })

    postContainer.on('click','a[data-action="deletePost"]',function (evt) {
        evt.preventDefault();
        var urlAction=$(this).attr("data-href");
        var btn=$(this);
        var post=$(this).closest(".post");
        var postTitle=post.attr("data-post-title");

        bootbox.confirm({
            message: 'Are you sure you want to delete post <b>' + postTitle + '</b>? You will not be able to recover it.',
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result){
                    $.ajax({
                        url:urlAction,
                        type:"POST",
                        success: function (success) {
                            if (success=="ok"){
                                post.hide();
                                alertDialog("Remove Post","Delete post success","success");
                            }else{
                                alertDialog("Remove Post",success,"error");
                            }
                        },
                        error: function (err) {
                            alertDialog("Remove Post","An error occurred is out of the process. Try again","error");
                            console.log(err.responseText);
                        }
                    });
                }else{
                    console.log("Cancel !!!");
                }
            }
        });
    });
});

function actionRole(obj){
    var action=$(obj).attr("data-role-action");
    var userId=$(obj).attr("data-user-id");
    $.ajax({
        url:'/blog/admin/actionroles',
        type:'POST',
        dataType:'json',
        contentType: "application/json",
        data: JSON.stringify({
            action: action,
            userId: userId
        }),
        success: function(data){
            if(data.status=="success"){
                location.reload();
            }else{
                console.log(data.status);
            }
        },
        error: function (err) {
            console.log(err.responseText);
        }
    })
}