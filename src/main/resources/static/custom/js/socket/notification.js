var stompClient=null;
$(document).ready(function () {
    connected();
    disconnect();
});

function connected() {
    var token = $("meta[name='_csrf']").attr("content");
    var headers = {};
    headers[ "X-CSRF-TOKEN"] = token;
    var socket=new SockJS("/blog/wss");
    var readpost=$("#readpost").val();
    stompClient = Stomp.over(socket);  
    stompClient.connect(headers,function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/wss/notification', function(messageOutput) {
            alertNotification(messageOutput);
        });
        stompClient.subscribe('/user/wss/notification',function(messageOut){
//            alertNotification(messageOut); 
                alertNotificationPersonal(messageOut);
        });
        if(readpost){
            var idPost=$("#pagePostsContent").find(".post").attr("data-post-id");
            stompClient.subscribe("/wss/posts/"+idPost,function(messageOut){
                processVoteViaSocket(messageOut.body); 
            });
            stompClient.subscribe("/wss/posts/comment/"+idPost,function(messageOut){
                processCommentViaSocket(messageOut.body); 
            });
            stompClient.subscribe("/wss/posts/comment/delete/"+idPost,function(messageOut){
                processDeleteCommentViaSocket(messageOut.body); 
            });
        }
    });
}
function disconnect() {
    if(stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function alertNotification(data){
    var obj=JSON.parse(data.body);
    console.log(obj);
    var urlPost="/blog/pósts/"+obj.id;
    var html='<li><a href="'+urlPost+'"><i class="fa fa-newspaper-o text-aqua"></i>'+obj.title+'</a></li>'
    $(".navbar-custom-menu").find(".menu").append(html);
    var countnotify=parseInt($("#countnotify").text())+1;
    $("#countnotify").text(countnotify);
    alertNotifyDialog('fa fa-newspaper-o','New article','A new article is created, read now <i class="fa fa-arrow-circle-o-right"></i>',urlPost);
}

function alertNotificationPersonal(data){
    var obj=JSON.parse(data.body);
    var postId=obj.postId;
    var userVote=obj.username;
    var voted=obj.vote;
    var action=obj.action;
    var countnotify=parseInt($("#countnotify").text()+0)/10+1;
    $("#countnotify").text(countnotify);
    if(action=="voteComment"){
        var urlPost="/blog/posts/"+postId+"/#commentList";
        var html='<li><a href="'+urlPost+'"><i class="fa fa-newspaper-o text-aqua"></i>Your Comment</a></li>'
        $(".navbar-custom-menu").find(".menu").append(html);
        if(voted=="like"){
            alertNotifyDialog("fa fa-thumbs-up","Comment",userVote+' like your comment. read now <i class="fa fa-arrow-circle-o-right"></i>',urlPost);
        }else{
            alertNotifyDialog("fa fa-thumbs-o-down","Comment",userVote+' dislike your comment. read now <i class="fa fa-arrow-circle-o-right"></i>',urlPost);
        }
    }else{
        var urlPost="/blog/posts/"+postId+"/#commentList";
        var html='<li><a href="'+urlPost+'"><i class="fa fa-newspaper-o text-aqua"></i>'+userVote+' </a></li>'
        $(".navbar-custom-menu").find(".menu").append(html);
        alertNotifyDialog("fa fa-thumbs-up","Comment",userVote+' ads also commented on the post, read now <i class="fa fa-arrow-circle-o-right"></i>',urlPost);
      
    }
}


