$(document).ready(function () {
    var comments=$("#commentList");
   
    comments.on('click','a[data-action="vote"]',function (evt) {
        var btn=$(this);
        var typeVote=btn.attr("data-vote-type");
        var idComment=btn.closest(".comment").attr("data-commentid");
        var idPost=$("#pagePostsContent").find(".post").attr("data-post-id");
        var action="voteComment";
        stompClient.send("/blogcooper-wss/wss/vote",{ },JSON.stringify({
            typeVote: typeVote,
            idComment: idComment,
            idPost:idPost,
            action:action
        }));
    });
    //create comment
   $(".comment-form").on('click','#sendComment',function(){
       var idPost=$("#pagePostsContent").find(".post").attr("data-post-id");
       var commentContent=$("#wmd-input").val();
       var action="createNewComment";
       stompClient.send("/blogcooper-wss/wss/comment",{ },JSON.stringify({
           idPost:idPost,
           commentContent: commentContent,
           parentComment:0,
           action:action
       }));
   });
   
   comments.on('click','a[data-action="deleteComment"]',function(evt){
       var idPost=$("#pagePostsContent").find(".post").attr("data-post-id");
       var idComment=$(this).closest('.comment').attr("data-commentid");
       bootbox.confirm("Are you sure you want to delete this comment?",function(result){
           if(result){
               stompClient.send("/blogcooper-wss/wss/comment/delete",{ },JSON.stringify({
                   postId: idPost,
                   commentId: idComment
               }));
           }
       });
   });
})

function processVoteViaSocket(messageOut){
    var obj=JSON.parse(messageOut);
    var idComment=obj.commentId;
    var typeVote=obj.vote;
    var comment=$("[data-commentid='"+idComment+"']");
    if(typeVote=="like"){
        comment.find(".like-arrow").addClass("likevoted");
        comment.find(".like-arrow").removeAttr("data-action");
        comment.find(".dislike-arrow").removeAttr("data-action");
        var votevalue=comment.find(".badge").text();
        votevalue=parseInt(votevalue,10)+1;
        comment.find(".badge").text(votevalue);
    }else{
        comment.find(".dislike-arrow").addClass("unlikevoted");
        comment.find(".like-arrow").removeAttr("data-action");
        comment.find(".dislike-arrow").removeAttr("data-action");
        var votevalue=comment.find(".badge").text();
        votevalue=parseInt(votevalue,10)-1;
        comment.find(".badge").text(votevalue);
    }
    
}
function processCommentViaSocket(data){
    var obj=JSON.parse(data);
    var adminAction='';

    if($("#userNameInUse").text()=="admin" || obj.userComment==$("#userNameInUse").text()){
        adminAction='<li>\n'
            +'<a href="javascript:void(0)" data-action="editComment" data-href="/blog/posts/'+obj.postId+'/comments/'+obj.commentId+'/edit"><span class="fa fa-edit"></span></a>\n'
            +'</li>'
            +'<li>\n'
            +'<a href="javascript:void(0)" data-action="deleteComment" data-href="/blog/posts/'+obj.postId+'/comments/'+obj.commentId+'/delete"><span class="fa fa-remove"></span></a>\n'
            +'</li>';
    }
    var commnetNode='<div class="comment-node">'
        + '<div class="comment row" data-commentid="'+obj.commentId+'" >'

        +'<div class="comment-header col-md-12 col-sm-12">'

        +'<a class="avatar" href="/blog/users/'+obj.userComment+'">'
        +'<img class="img-rounded" width="28" height="28" src="/blog/uploads/avatars/'+obj.userImage+'">'
        +'</a>'
        +'<a class="comment-username" href="/blog/users/'+obj.userComment+'"> '+obj.userComment+'</a>'


        +'<span class="post-date">'
        +'<span>'+obj.dateTime+'</span>'

        +'</span>'
        +'</div>'

        +'<div class="col-md-12 col-sm-12">'
        +'<div class="comment-content"><p>'+obj.contentComment+'</p></div>'
        +'</div>'

        +'<div class="post-actions col-md-12 col-sm-12">'
        +'<ul class="nav nav-pills">'

        +'<li>'
        +'<a class="rating-arrow like-arrow" href="javascript:void(0)" data-vote-type="like" data-action="vote" data-href="/blog/posts/'+obj.postId+'/comments/'+obj.commentId+'/like">'
        +'<i class="fa fa-thumbs-o-up"></i>'
        +'</a>'
        +'</li>'
        +'<li><a href="#"><i class="rating-value badge bg-green rating-value-no">0</i></a>'
        +'</li>'
        +'<li><a class="rating-arrow dislike-arrow" href="javascript:void(0)" data-vote-type="dislike" data-action="vote" data-href="/blog/posts/'+obj.postId+'/comments/'+obj.commentId+'/dislike">'
        +'<i class="fa fa-thumbs-o-down"></i>'
        +'</a>'
        +'</li>'


        +'<li>'
        +'<a class="comment-reply-btn" href="javascript:void(0)" data-action="addReply" data-href="/blog/posts/'+obj.posId+'/comments/create"><i class="fa fa-reply"></i>'
        +'</a>'
        +'</li>'
        +'<li>'

        +'</li>'
        +'<li>'

        +'</li>'
        +adminAction
        +'</ul>'
        +'</div>'
        +'<div class="loading-indicator" style="display: none">'
        +'<img src="/blog/images/ajax-loader.gif">'
        +'</div>'
        +'</div>'
        +'<div class="child-comment-node">'

        +'</div>'
        +'</div>'
    $('#commentList').prepend(commnetNode);
}
function processDeleteCommentViaSocket(data){
    var dataRec=data.split("-");
    if(dataRec[0]=="ok"){
        var idComment=dataRec[1];
        $("#commentList").find("[data-commentid="+idComment+"]").parent().hide();
        if($("#userNameInUse").text()=="admin"||obj.userComment==$("#userNameInUse").text()){
            alertDialog("Delete Comment",dataRec[1],'success');
        }
    }else{
        if($("#userNameInUse").text()=="admin"||obj.userComment==$("#userNameInUse").text()){
            alertDialog("Delete Comment",dataRec[1],'error');
        }
    }
}
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
}