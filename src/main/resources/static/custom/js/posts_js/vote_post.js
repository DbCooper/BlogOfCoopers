/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
   var pageContent=$("#pagePostsContent");
   pageContent.on('click','a[data-action="vote"]',function (evt) {
      var btn=$(this);
      var href=btn.attr("data-href");
      var typeVote=btn.attr("data-vote-type");
      var idPost=btn.closest(".post").attr("data-post-id");
      $.ajax({
            url:href,
            type:'POST',
            success: function (success) {
                if (success=="ok"){
                    if (typeVote=='like'){
                        btn.addClass('likevote');
                        var postratingblock = btn.closest(".post-rating-block");
                        var currentVal=postratingblock.find(".rating-value .badge").text();
                        currentVal++;
                        postratingblock.find(".rating-value .badge").text(currentVal);
                        postratingblock.find(".dislike-arrow").removeAttr("data-action");
                        alertDialog(typeVote.toUpperCase(),"Success ",'success');

                    }else{
                        btn.addClass('unlikevote');
                        var postratingblock = btn.closest(".post-rating-block");
                        var currentVal=postratingblock.find(".badge").text();
                        currentVal=currentVal-1;
                        postratingblock.find(".rating-value .badge").text(currentVal);
                        postratingblock.find(".like-arrow").removeAttr("data-action");
                        alertDialog(typeVote.toUpperCase(),"Success",'success');

                    }
                }else{
                    alertDialog(typeVote.toUpperCase(),success,'error');
                }
            },
            error: function (err) {
                console("Error",err.responseText);
            }
      });
   });
});


